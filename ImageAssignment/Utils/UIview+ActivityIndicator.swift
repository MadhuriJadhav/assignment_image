//
//  ViewControllerUtils.swift
//  ImageAssignment
//
//  Created by Madhuri on 11/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import UIKit

extension UIView {
    // MARK: - Extension to show Spinner on View
    func showSpinner() {
        let backgroundView = UIView()
        backgroundView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        backgroundView.backgroundColor = UIColor(white: 0, alpha: 0.7)
        backgroundView.tag = 475647
        backgroundView.center = self.center
        backgroundView.layer.cornerRadius = 10

        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
        activityIndicator.center = CGPoint(x: backgroundView.frame.size.width / 2, y:
               backgroundView.frame.size.height / 2)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .white
        activityIndicator.startAnimating()
        backgroundView.addSubview(activityIndicator)
        self.addSubview(backgroundView)
    }

    func hideSpinner() {
        if let background = viewWithTag(475647) {
            background.removeFromSuperview()
        }
    }
}
